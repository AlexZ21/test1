#ifndef AREA_H
#define AREA_H

#include <vector>
#include <thread>
#include <chrono>
#include <random>
#include <mutex>
#include <atomic>

class Area
{
public:
    struct Rook {
        Rook() : id(0), position(std::make_pair(-1, -1)), stepToWait(0), stepCount(0), wait(false) {}
        Rook(const Rook &other) {
            id = other.id;
            position = other.position;
            stepToWait = other.stepToWait;
            stepCount.store(other.stepCount.load());
            wait.store(other.wait.load());
        }

        size_t id; ///< Номер ладьи
        std::pair<int32_t, int32_t> position; ///< Текущая позиция ладьи
        int32_t stepToWait; ///< Количество шагов до ожидания
        std::atomic_int32_t stepCount; ///< Количество оставшихся шагов
        std::atomic_bool wait; ///< Находится в ожидании свободной ячейки
    };

    Area(size_t rookCount = 6);
    ~Area() = default;

    //! Запустить обработку фигур
    void run();
    //! Остановить обработку
    void stop();

    //! Получить список фигур
    const std::vector<Rook> &rooks() const;

private:
    //! Сбросить состояние поля
    void resetArea();
    //! Получить свободную ячейку в поле
    std::pair<int32_t, int32_t> getFreeCell();
    //! Получить координаты новой позиции фигуры
    std::pair<int32_t, int32_t> getTargetCell(const std::pair<int32_t, int32_t> &cell);
    //! Свободна ли ячейка
    bool isCellFree(const std::pair<int32_t, int32_t> &cell);
    //! Свободен ли путь для перемещения фигуры
    bool moveIfPathFree(const std::pair<int32_t, int32_t> &fromCell, const std::pair<int32_t, int32_t> &toCell);
    //! Валидна ли ячейка с заданными координатами
    bool isCellValid(const std::pair<int32_t, int32_t> &cell);

private:
    size_t m_rookCount; ///< Количество фигур
    std::vector<Rook> m_rooks; ///< Список текущих фигур
    std::vector<std::thread> m_threads; ///< Список потоков для обработки фигур

    std::vector<std::vector<bool>> m_area; ///< Поле
    std::mutex m_areamx; ///< Мьютекс для защиты ячеек от совмстного изменения

    std::mt19937 m_randomEngine;
};

#endif // AREA_H
