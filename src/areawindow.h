#ifndef AREAWINDOW_H
#define AREAWINDOW_H

#include "area.h"

#include <QWidget>
#include <QTimer>

class AreaWindow : public QWidget
{
    Q_OBJECT
public:
    explicit AreaWindow(QWidget *parent = nullptr);
    ~AreaWindow();

    void run();

protected:
    void paintEvent(QPaintEvent *);

private:
    Area m_area;
    std::thread m_thread;
    QTimer m_updateTimer;

};

#endif // AREAWINDOW_H
