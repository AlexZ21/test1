#include "area.h"

#include <ctime>
#include <condition_variable>
#include <iostream>

Area::Area(size_t rookCount) :
    m_rookCount(rookCount),
    m_randomEngine(std::time(nullptr))
{
}

void Area::run()
{
    resetArea();
    m_rooks.clear();
    // Создание фигур с рандомными позициями
    for (size_t i = 0; i < m_rookCount; ++i) {
        Rook rook;
        rook.id = i;
        rook.stepToWait = 2;
        rook.stepCount.store(50);
        rook.position = getFreeCell();
        // Если после 5 попыток найти свободную ячейку, ячейка не найдена,
        // то пропускается фигура
        if (rook.position.first < 0 || rook.position.second < 0)
            continue;
        // Помечаем ячейку, что она занята
        m_area[rook.position.first][rook.position.second] = true;
        std::cout << "Create rook at <" << rook.position.first << ", " << rook.position.second << ">" << std::endl;
        m_rooks.push_back(rook);
    }

    for (Rook &rook : m_rooks) {
        m_threads.push_back(std::thread([this, &rook](){
            while (rook.stepCount.load() > 0) {
                std::pair<int32_t, int32_t> targetPosition = getTargetCell(rook.position);
                if (!isCellValid(targetPosition))
                    continue;
                // Если ячейка не свободна, то каждые 100 миллисекунд
                // проверяется, освободилась ли ячейка в течении 5 секунд.
                // Если нет, то препренимается попытка поиска новой ячейки.
                rook.wait.store(true);
                bool result = false;
                std::chrono::system_clock::time_point tp = std::chrono::system_clock::now();
                std::chrono::duration<float> dur = std::chrono::system_clock::now() - tp;
                while (dur.count() < 5 && rook.stepCount.load() > 0) {
                    // Проверить, свободен ли путь до ячейки и если да, то занять ее и освободить текущую
                    result = moveIfPathFree(rook.position, targetPosition);
                    if (result) {
                        rook.wait.store(false);
                        rook.position = targetPosition;
                        --rook.stepCount;
                        break;
                    }
                    std::this_thread::sleep_for(std::chrono::milliseconds(100));
                    dur = std::chrono::system_clock::now() - tp;
                }

                if (!result)
                    continue;

                --rook.stepToWait;
                if (rook.stepToWait == 0) {
                    rook.stepToWait = 2;
                    std::uniform_int_distribution<int32_t> d(200, 300);
                    std::this_thread::sleep_for(std::chrono::milliseconds(d(m_randomEngine)));
                }
            }
        }));
    }

    for (std::thread &thread : m_threads) {
        if (thread.joinable())
            thread.join();
    }

    m_threads.clear();
    std::cout << "Finish" << std::endl;
}

void Area::stop()
{
    for (Rook &rook : m_rooks)
        rook.stepCount.store(0);
}

const std::vector<Area::Rook> &Area::rooks() const
{
    return m_rooks;
}

void Area::resetArea()
{
    m_area.clear();
    for (size_t i = 0; i < 10; ++i)
        m_area.push_back(std::vector<bool>(10, false));
}

std::pair<int32_t, int32_t> Area::getFreeCell()
{
    std::pair<int32_t, int32_t> position = std::make_pair(-1, -1);
    int32_t tryCount = 5;
    std::uniform_int_distribution<int32_t> d(0, 9);
    while (tryCount--) {
        position = std::make_pair(d(m_randomEngine), d(m_randomEngine));
        if (isCellFree(position))
            return position;
    }
    return position;
}

std::pair<int32_t, int32_t> Area::getTargetCell(const std::pair<int32_t, int32_t> &cell)
{
    std::pair<int32_t, int32_t> position = std::make_pair(-1, -1);
    if (!isCellValid(cell))
        return position;
    position = cell;
    std::uniform_int_distribution<int32_t> d(0, 9);
    std::uniform_int_distribution<int32_t> d2(0, 1);
    // Поиск новой ячейки, не совпадающей
    while (position == cell) {
        position = std::make_pair(d(m_randomEngine), d(m_randomEngine));
        int32_t direction = d2(m_randomEngine);
        switch (direction) {
        case 0: // Horizontal
            position.first = cell.first;
            break;
        case 1: // Vertical
            position.second = cell.second;
            break;
        }
    }
    return position;
}

bool Area::isCellFree(const std::pair<int32_t, int32_t> &cell)
{
    return isCellValid(cell) ? !m_area[cell.first][cell.second] : false;
}

bool Area::moveIfPathFree(const std::pair<int32_t, int32_t> &fromCell, const std::pair<int32_t, int32_t> &toCell)
{
    std::lock_guard<std::mutex> lock(m_areamx);

    if (!isCellValid(fromCell) || !isCellValid(toCell))
        return false;

    std::pair<int32_t, int32_t> minCell = std::min(fromCell, toCell);
    std::pair<int32_t, int32_t> maxCell = std::max(fromCell, toCell);

    for (int32_t i = minCell.first; i <= maxCell.first; ++i) {
        for (int32_t j = minCell.second; j <= maxCell.second; ++j) {
            std::pair<int32_t, int32_t> cell = std::make_pair(i, j);
            if (cell == fromCell)
                continue;
            if (!isCellFree(cell))
                return false;
        }
    }

    m_area[fromCell.first][fromCell.second] = false;
    m_area[toCell.first][toCell.second] = true;
    return true;
}

bool Area::isCellValid(const std::pair<int32_t, int32_t> &cell)
{
    return !(cell.first < 0 || cell.second < 0);
}

