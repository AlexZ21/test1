#include "area.h"
#include "areawindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    AreaWindow w;
    w.show();
    w.run();

    return a.exec();
}
