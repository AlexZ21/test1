#include "areawindow.h"

#include <QPainter>

AreaWindow::AreaWindow(QWidget *parent) :
    QWidget(parent)
{
    setFixedSize(400, 400);
    connect(&m_updateTimer, &QTimer::timeout,
            this, static_cast<void(AreaWindow::*)()>(&AreaWindow::repaint));
    m_updateTimer.setInterval(10);
    m_updateTimer.start();
}

AreaWindow::~AreaWindow()
{
    m_area.stop();
    if (m_thread.joinable())
        m_thread.join();
}

void AreaWindow::run()
{
    m_thread = std::thread([this](){
        m_area.run();
    });
}

void AreaWindow::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);

    QRect viewPortRect(4, 4, width() - 8, height() - 8);

    QPen pen1;
    pen1.setWidth(2);
    p.setPen(pen1);

    p.drawRect(viewPortRect);

    int32_t cellSize = viewPortRect.width() / 10;

    int32_t colorAlpha = 100;
    std::vector<QColor> colors;
    colors.push_back(QColor(26, 188, 156, colorAlpha));
    colors.push_back(QColor(243, 156, 18, colorAlpha));
    colors.push_back(QColor(46, 204, 113, colorAlpha));
    colors.push_back(QColor(52, 152, 219, colorAlpha));
    colors.push_back(QColor(231, 76, 60, colorAlpha));
    colors.push_back(QColor(211, 84, 0, colorAlpha));
    colors.push_back(QColor(142, 68, 173, colorAlpha));
    colors.push_back(QColor(52, 73, 94, colorAlpha));

    // Отрисовка фигур
    for (const Area::Rook &rook : m_area.rooks()) {
        std::pair<int32_t, int32_t> position = rook.position;
        p.fillRect(QRect(viewPortRect.x() + position.first * cellSize,
                         viewPortRect.y() + position.second * cellSize,
                         cellSize, cellSize),
                   colors[rook.id % colors.size()]);
        p.drawText(QRect(viewPortRect.x() + position.first * cellSize,
                         viewPortRect.y() + position.second * cellSize, cellSize, cellSize),
                   Qt::AlignCenter, QString::number(rook.id));

        QFont f;
        f.setPixelSize(9);
        p.setFont(f);

        if (rook.wait.load()) {
            int32_t waitTextHeight = 12;
            p.drawText(QRect(viewPortRect.x() + position.first * cellSize,
                             viewPortRect.y() + (position.second + 1) * cellSize - waitTextHeight - 2,
                             cellSize, waitTextHeight),
                       Qt::AlignCenter, "wait...");
        }

        int32_t finishTextHeight = 12;
        p.drawText(QRect(viewPortRect.x() + position.first * cellSize,
                         viewPortRect.y() + position.second * cellSize,
                         cellSize, finishTextHeight),
                   Qt::AlignCenter, "Steps: " + QString::number(rook.stepCount.load()));

        if (rook.stepCount.load() == 0) {
            p.drawText(QRect(viewPortRect.x() + position.first * cellSize,
                             viewPortRect.y() + (position.second + 1) * cellSize - finishTextHeight - 2,
                             cellSize, finishTextHeight),
                       Qt::AlignCenter, "finish");
        }
    }

    // Отрисовка сетки
    for (int32_t i = 0; i < 10; ++i) {
        p.drawLine(QPoint(viewPortRect.x(), viewPortRect.y() + i * cellSize),
                   QPoint(viewPortRect.width() + viewPortRect.x(), viewPortRect.y() + i * cellSize));
        p.drawLine(QPoint(viewPortRect.x() + i * cellSize, viewPortRect.y()),
                   QPoint(viewPortRect.x() + i * cellSize, viewPortRect.height() + viewPortRect.y()));
    }

}
